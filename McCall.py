# -*- coding: utf-8 -*-
"""
Created on Thu Feb 18 16:13:52 2016

@author: David Evans
"""

import numpy as np

def bellmanTmap(beta,w,pi,b,alpha,chi,Vcont,Qcont):
    '''
    Iterates of Bellman Equation in Problem 1
    See Homework 3.pdf for Documentation
    '''
    pi_dot_V = pi.dot(Vcont)
    S = len(w)
    V = np.zeros(S)
    
    Q_ns =  b + beta*(0.3*pi_dot_V + .7*Qcont)
    Q_s =  b - chi + beta*(.5*pi_dot_V + .5*Qcont)
    Q = np.maximum(Q_ns,Q_s)
    
    V_a = w + beta*(alpha*Qcont + (1-alpha)*Vcont)
    V = np.maximum(V_a, Q)
    
    C_bool = (V==V_a)
    C= 1*C_bool
    
    E = bool(Q_s > Q_ns)
    
    return V,Q,C,E
    
      
    
    
def solveInfiniteHorizonProblem(beta,w,pi,b,alpha,chi,epsilon=1e-8):
    '''
    Solve infinite horizon workers problem in Problem 1
    See Homework 3.pdf for Documentation
    '''
    Q=0
    S=len(w)
    V=np.zeros(S)
    Vnew,Qnew,C,E = bellmanTmap(beta,w,pi,b,alpha,chi,V,Q)
    while np.linalg.norm(V-Vnew)>epsilon:
        V=Vnew
        Q=Qnew
        Vnew,Qnew,C,E = bellmanTmap(beta,w,pi,b,alpha,chi,V,Q)
    return Vnew, Qnew, C, E
    
def HazardRate(pi,C,E):
    '''
    Computes the hazard rate of leaving unemployment in Problem 1
    See Homework 3.pdf for Documentation
    '''
    if E:
        wp = .5
    else:
        wp = .3
    
    Hrate = wp*(pi.dot(C))
    return (Hrate)
    

        
        
def bellmanTmap_HC(beta,w,pi,b,alpha,p_h,h,Vcont,Qcont):
    '''
    Iterates of Bellman Equation of Problem 2
    See Homework 3.pdf for Documentation
    '''
    
    J = len(Qcont)
    S = len(Vcont[1,:])
    
    Q = np.zeros(J)
    V = np.zeros((J,S))
    Vemp = np.zeros((J,S))
    C = np.zeros((J,S))
        
    
    Q[0] = b + beta*(pi.dot(Vcont[0,:]))
    for j in range(1,J):
        Q[j] = b + beta*(p_h*(pi.dot(Vcont[j-1,:]))+(1-p_h)*(pi.dot(Vcont[j,:])))
        
    for j in range(J-1):
        Vf = p_h*Qcont[j+1] + (1-p_h)*Qcont[j]
        Vnf   = p_h*Vcont[j+1,:] + (1-p_h)*Vcont[j,:]
        Vemp[j,:] = w*h[j] + beta*(alpha*Vf + (1-alpha))
        V[j,:] = np.maximum(Vemp[j,:],np.ones(S)*Q[j])
        
    Vf = p_h*Qcont[J-1] + (1-p_h)*Qcont[J-1]
    Vnf   = p_h*Vcont[J-1,:] + (1-p_h)*Vcont[J-1,:]
    Vemp[J-1,:] = w*h[J-1] + beta*(alpha*Vf + (1-alpha)*Vnf)
    V[J-1,:] = np.maximum(Vemp[J-1,:],np.ones(S)*Q[J-1])
        
    for j in range(J):
        for s in range(S):
            if V[j,s] == Vemp[j,s]:
                C[j,s] = 1
                
    return V, Q, C
    

def solveInfiniteHorizonProblem_HC(beta,w,pi,b,alpha,p_h,h,epsilon=1e-8):
    '''
    Solve infinite horizon workers problem of Problem 2
    See Homework 3.pdf for Documentation
    '''
    S = len(w)
    J = len(h)
    
    V = np.zeros((J,S))
    Q = np.zeros(J)
    C = np.zeros((J,S))
    
    Vnew, Qnew, Cnew = bellmanTmap_HC(beta,w,pi,b,alpha,p_h,h,V,Q)
    
    while (np.linalg.norm(V-Vnew)>epsilon):
        V,Q,C = Vnew,Qnew,Cnew
        Vnew,Qnew,Cnew = bellmanTmap_HC(beta,w,pi,b,alpha,p_h,h,V,Q)
    return Vnew,Qnew,Cnew

    
    
    
    
    
    
    
    